const chai = require ('chai');
const chaiHttp = require ('chai-http');
const {after, before, describe , it} = require ('mocha');

const server = require ('./index');
let expect = chai.expect;

chai.should ();
chai.use (chaiHttp);

describe('Test Connection', () => {
  it ('check Connection', done => {
    chai.request (server).get('/api').end ((err, res) => {
      if (err) {
        done (err);
        process.exit (1);
      } else {
        
        res.should.have.status (200);
        done ();
      }
    });
  });
});
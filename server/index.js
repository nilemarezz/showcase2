const express = require('express')
const app = express()
const port = 3000;

app.get('/api',(req,res)=>{
    res.status(200).send(`nile nile nile`)
})

if (process.env.NODE_ENV !== 'test') {
    app.listen(port);
  }

module.exports = app;